﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LANREG
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			// URL RE-WRITING: http://stackoverflow.com/questions/2375256/url-rewriting-in-net-mvc

			/* TESTING */
			routes.MapRoute(
				name: "IPNTesting",
				url: "IPN",
				defaults: new { controller = "LayoutTESTING", action = "IPN" }
			);

			/* HOME LEVEL STUFF */

			routes.MapRoute(
				name: "CreateOrganization",
				url: "NewGroup",
				defaults: new { controller = "Organization", action = "Create" }
			);

			routes.MapRoute(
				name: "CreateParty",
				url: "NewEvent",
				defaults: new { controller = "Party", action = "Create" }
			);

			//TODO: remove this, create layout should only be available from a group.
			routes.MapRoute(
				name: "CreateLayoutHome",
				url: "NewLayout",
				defaults: new { controller = "Layout", action = "Create" }
			);

			routes.MapRoute(
				name: "ViewOrganizations",
				url: "Groups",
				defaults: new { controller = "Organization", action = "Index" }
			);

			routes.MapRoute(
				name: "ViewParties",
				url: "Events",
				defaults: new { controller = "Party", action = "Index" }
			);

			/* ACCOUNT LEVEL STUFF */
			// /Account/*


			/* ORGANIZATION LEVEL STUFF */
			// /{org_url}

			routes.MapRoute(
				name: "OrganizationHome",
				url: "{org_url}",
				defaults: new { controller = "Organization", action = "Details" }
			);

			// /{org_url}/Events

			routes.MapRoute(
				name: "OrganizationEvents",
				url: "{org_url}/Events",
				defaults: new { controller = "Organization", action = "Events" }
			);

			// /{org_url}/Admin/*


			/* EVENT LEVEL STUFF */
			// /{org_url}/Event/{party_url}

			routes.MapRoute(
				name: "PartyHome",
				url: "{org_url}/Event/{party_url}",
				defaults: new { controller = "Party", action = "Details" }
			);

			// /{org_url}/Event/{party_url}/Seating

			routes.MapRoute(
				name: "PartySeating",
				url: "{org_url}/Event/{party_url}/Seating",
				defaults: new { controller = "Layout", action = "Details" }
			);

			/* ALL ELSE FAILS DEFAULT STUFF */
			//TODO: remove this at some point

			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}