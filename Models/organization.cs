//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LANREG.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class organization
    {
        public organization()
        {
            this.layouts = new HashSet<layout>();
            this.parties = new HashSet<party>();
            this.org_access = new HashSet<org_access>();
        }
    
        public int id { get; set; }
        public string name { get; set; }
        public string org_url { get; set; }
        public string description { get; set; }
        public string location { get; set; }
    
        public virtual ICollection<layout> layouts { get; set; }
        public virtual ICollection<party> parties { get; set; }
        public virtual ICollection<org_access> org_access { get; set; }
    }
}
