﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LANREG.Models {
	public class HomeModel {
		public HomeModel() {

		}
		public HomeRightColumnModel RightColumn { get; set; }
	}

	public class HomeRightColumnModel {
		public HomeRightColumnModel() {
			organizations = new List<organization>();
			parties = new List<party>();

		}
		public List<organization> organizations { get; set;}
		public List<party> parties { get; set; }
	}
}