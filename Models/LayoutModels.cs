﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LANREG.Models {
	public class LayoutModels {
	}

	//TODO: ensure that these are needed

	[Serializable]
	public class SaveLayoutModel {
		public int tableStep;
		public int seatStep;
		public int seatCost;
		public int orgID;
		public string name;
		public List<table> tables;
	}

	public class SelectSeatingModel {
		public List<table> tables;
		public seat tblCurrSeat;
		public seat_user currSeatUser;
		public user currUser;
		public int layoutID;
		public int partyID;
	}
}