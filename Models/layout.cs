//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LANREG.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class layout
    {
        public layout()
        {
            this.parties = new HashSet<party>();
            this.seat_user = new HashSet<seat_user>();
            this.tables = new HashSet<table>();
        }
    
        public int id { get; set; }
        public string name { get; set; }
        public int numSeats { get; set; }
        public int org_id { get; set; }
        public bool shared { get; set; }
    
        public virtual organization organization { get; set; }
        public virtual ICollection<party> parties { get; set; }
        public virtual ICollection<seat_user> seat_user { get; set; }
        public virtual ICollection<table> tables { get; set; }
    }
}
