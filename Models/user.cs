//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LANREG.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class user
    {
        public user()
        {
            this.payments = new HashSet<payment>();
            this.seat_user = new HashSet<seat_user>();
            this.org_access = new HashSet<org_access>();
        }
    
        public int id { get; set; }
        public string username { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string pass { get; set; }
        public string email { get; set; }
    
        public virtual ICollection<payment> payments { get; set; }
        public virtual ICollection<seat_user> seat_user { get; set; }
        public virtual ICollection<org_access> org_access { get; set; }
    }
}
