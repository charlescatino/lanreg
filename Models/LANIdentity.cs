﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace LANREG {
	public class LANIdentity : IIdentity {
		private System.Web.Security.FormsAuthenticationTicket ticket;

		public LANIdentity(System.Web.Security.FormsAuthenticationTicket ticket) {
			this.ticket = ticket;
		}

		public bool isAuthenticated {
			get { return true; }
		}

		public string Name {
			get { return ticket.Name; }
		}

		public string GamerTag {
			get { return ticket.UserData; }
		}
	}
}