//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LANREG.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class seat
    {
        public seat()
        {
            this.seat_user = new HashSet<seat_user>();
        }
    
        public int id { get; set; }
        public int tableID { get; set; }
        public string name { get; set; }
        public decimal cost { get; set; }
    
        public virtual ICollection<seat_user> seat_user { get; set; }
        public virtual table table { get; set; }
    }
}
