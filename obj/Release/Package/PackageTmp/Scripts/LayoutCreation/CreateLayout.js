﻿/* 
CreateLayout

Used to handle all the Javascript stuff for creating and arranging table layouts

*/

var startTableID = 74813; // 'TABLE'
var numTables = 0;
var yGrid = 20;
var xGrid = 20;
tables = new Array();
var currTableIdDelete;
var actionDiv = document.getElementById('delTableDiv'); // $('#delTableDiv');
//layout.sortedTable = new Array();

window.onload = function () {
	// reserved
}

function addTable() {
	var strOrientation = $('#tableOrientationDDL option:selected').text();
	var tableLength = $("#tableLengthDDL option:selected").text();
	var tableWidth = $("#tableWidthDDL option:selected").text();
    placeTable(tableLength, tableWidth, strOrientation, 0, 0);
    $("#submit").disabled = false;
}

function placeTable(length, width, orientation, xStart, yStart) {
    row = new Array();
    cell = new Array();
    row_num = 0;
    cell_num = 0;
    if (orientation == "Horizontal") {
        row_num = width;
        cell_num = length;
    }
    else {
        row_num = length;
        cell_num = width;
    }
    tab = document.createElement('table');
    tab.setAttribute('id', startTableID + numTables);
    tab.setAttribute('class', 'tableDrag ui-draggable');
    tab.setAttribute('onmousedown', 'setPosition(this)');
    tab.setAttribute('onmouseover', 'hoverTable(this)');
    tab.setAttribute('onmouseout', 'hoverTableOut(this)');
    tab.setAttribute('style', 'left: ' + xStart + ' px; top: ' + yStart + ' px; position: absolute;');
    //tab.setAttribute('id', 'newtable');
    tbo = document.createElement('tbody');
    for (i = 0; i < row_num; i++) {
        row[i] = document.createElement('tr');
        for (j = 0; j < cell_num; j++) {
            cell[j] = document.createElement('td');
            cell[j].setAttribute('class', 'tdDrag');
            //cont = document.createTextNode((i + 1) * (j + 1));
            //cell[j].appendChild(cont);
            row[i].appendChild(cell[j]);
        }
        tbo.appendChild(row[i]);
    }
    tab.appendChild(tbo);
    $(tab).hide();

    $('#layoutArea').append(tab);
    $(tab).fadeIn(1000);
    //document.getElementById('layoutArea').appendChild(tab);

    tables.push(new Table(orientation, length, width, startTableID + numTables));
    tables[tables.length - 1].setCoords(0, 0);

    numTables++;
    initDraggable(tab);
}

function initDraggable(control) {
	$("#" + control.id).draggable({ zIndex: 2700 });
	$("#" + control.id).draggable({ grid: [xGrid, yGrid] });
}

function hoverTable(control) {
	currTableIdDelete = control.id;
	control.style.backgroundColor = 'green';

	var oTop = 0, oLeft = getTopRight(control.id);

	// prepare position
	do {
		oLeft += control.offsetLeft;
		oTop += control.offsetTop;
	} while (control = control.offsetParent);

	actionDiv.style.top = (oTop + 20) + 'px';
	actionDiv.style.left = (oLeft - 10) + 'px';

	actionDiv.style.visibility = 'visible';
}

function getTopRight(tableID) {
	if (tables[indexOfTable(tableID)].orientation == 'Vertical') {
		return parseInt(tables[indexOfTable(tableID)].width * 20);
	} else {
		return parseInt(tables[indexOfTable(tableID)].length * 20);
	}
}

function hoverTableOut(control) {
	control.style.backgroundColor = '';
}

function deleteTable() {
	var tableToDelete = currTableIdDelete;
	$('#' + tableToDelete).fadeOut(1000, function () {
		$('#' + tableToDelete).remove();
		tables.splice(indexOfTable(tableToDelete), 1);
		actionDiv.style.visibility = 'hidden';
	});
	actionDiv.style.visibility = 'hidden';
}


// Written by Quintin Cummins
function setPosition(control) {
    control.style.backgroundColor = "black";
    $("#" + control.id).draggable({ zIndex: 2700 });
    $("#" + control.id).draggable({ grid: [xGrid, yGrid] });
    $("#" + control.id).draggable({
        stop: function (event, ui) {
            Stoppos = $(this).position();
            $("#box2").val(Stoppos.left + "," + Stoppos.top);
            control.style.backgroundColor = "white";
            // update table in array with the proper coordinates
            tables[indexOfTable(control.id)].setCoords(Stoppos.left, Stoppos.top);
        }
    });
}

/* Iterate through tables and return the index of the table 
 * for the given tableID (the HTML ID tag of the table
 */
function indexOfTable(tableID) {
    for (var i=0; i < tables.length; i++) {
        if (tables[i].id == tableID)
        return i;
    }
}

/* This function clears all of the tables from the Layout Area div */
function clearTables() {
    var i;
    $("#layoutArea").fadeOut(500, function () {
    	$("#layoutArea").html("");
    	$("#layoutArea").fadeIn();
    	tables.length = 0;
    });
    $("#submit").disabled = true;
    actionDiv.style.visibility = 'hidden';
}

/* Populate my layout with pertinent data, JSONify, send to
 * the C# backend to store table data
 */
function saveLayout() {
	if (!isValidInput()) {
		alert("Invalid input of some kind");
		return false;
	}

	var layout = new Object();

	layout.tables = tables;

	// add all other attributes to the layout object
	layout.tableStep = $('#tableStepDDL option:selected').val();
	layout.seatStep = $('#seatStepDDL option:selected').val();
	layout.name = $('#layoutNameTB').val();
	layout.seatCost = $('#seatCostTB').val();
	layout.orgID = $('#userOrgsOwnedDDL').val();

	// sort the tables by position (leftOffset), then (topOffset)
	layout.tables.sort(sort_by('leftOffset', 'topOffset'));

	// store layout in JSON object
	var layoutString = window.JSON.stringify(layout);

	$.post('/Layout/Create',
		{ jsonData: layoutString }, function (msg) {
			if (msg == 'True') {
				alert('Your layout was created successfully!');
				window.location.replace('/' + layout.orgID);
			} else {
				alert('There was an error while creating your layout. =[');
			}
		}
	);
    return true;
}

function isValidInput() {
	// check to make sure there are tables
	if (tables.length == 0)
		return false;
	// Ensure a name is specified
	if ($('#newLayout_name').text == "")
		return false;
	// Need a numeric value 
	if ($('#seatCostTB').text == "" || !isNaN($('#seatCostTB').text))
		return false;
	return true;
}

/* Sorting function stuff used to sort the tables
 * Found here:
 * http://stackoverflow.com/questions/6913512/how-to-sort-an-array-of-objects-by-multiple-fields
 */

var sort_by;

(function () {
    // utility functions
    var default_cmp = function (a, b) {
        if (a == b) return 0;
        return a < b ? -1 : 1;
    },
        getCmpFunc = function (primer, reverse) {
            var dfc = default_cmp, // closer in scope
                cmp = default_cmp;
            if (primer) {
                cmp = function (a, b) {
                    return dfc(primer(a), primer(b));
                };
            }
            if (reverse) {
                return function (a, b) {
                    return -1 * cmp(a, b);
                };
            }
            return cmp;
        };

    // actual implementation
    sort_by = function () {
        var fields = [],
            n_fields = arguments.length,
            field, name, reverse, cmp;

        // preprocess sorting options
        for (var i = 0; i < n_fields; i++) {
            field = arguments[i];
            if (typeof field === 'string') {
                name = field;
                cmp = default_cmp;
            }
            else {
                name = field.name;
                cmp = getCmpFunc(field.primer, field.reverse);
            }
            fields.push({
                name: name,
                cmp: cmp
            });
        }

        // final comparison function
        return function (A, B) {
            var a, b, name, result;
            for (var i = 0; i < n_fields; i++) {
                result = 0;
                field = fields[i];
                name = field.name;

                result = field.cmp(A[name], B[name]);
                if (result !== 0) break;
            }
            return result;
        }
    }
} ());


/* Debug stuff below here... */
function showPosition(control) {
    var left;
    var top;
    var i;
    for (i = 0; i < tables.length; i++) {
    	var left = document.getElementById(startTableID + i).offsetLeft - startX;
    	var top = document.getElementById(startTableID + i).offsetTop - startY;
        alert(left + ", " + top);
    }
}

function addText() {
    var txt = 'my new text';
    var newText = document.createTextNode(txt);
    //alert(txt);
    document.getElementByID('MainContent_myPanel').appendChild(txt);
}