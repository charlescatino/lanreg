﻿// define reference to the hidden div element
var div;
//var formDiv;
var lastSeat;
//var startX, startY;
seats = new Array();
var totalPrice = 0;

// show hoverbox w/ info
function seatinfo_show(obj, tableName, seatName, username, paid) {
	div = document.getElementById("hoverbox");
    var oTop = 0, oLeft = 0;

    // prepare position
    do {
        oLeft += obj.offsetLeft;
        oTop += obj.offsetTop;
    } while (obj = obj.offsetParent);

    // set the position of invisible div
    div.style.top = (oTop + 20) + 'px';
    div.style.left = (oLeft + 20) + 'px';

    div.innerHTML = "Seat: " + tableName + "-" + seatName;
    if (username != null)
        div.innerHTML += "<br>" + username;
    if (paid == "false")
        div.innerHTML += "<br>RESERVED";

    // show hoverbox
    div.style.visibility = 'visible';
}

// hide hoverbox (hide div element)
function seatinfo_hide() {
    div.style.visibility = 'hidden';
}

// User selects a seat
function seatClicked(seatID, userID, layoutID, partyID, elementID, userReserved) {
	$.post('/Seat/Reserve',
		{ "seatID": seatID, "userID": userID, "layoutID": layoutID, "partyID": partyID },
		function (data) {
			if (data) { // successful.
				$(elementID).removeAttr("onclick");
				var onClickFunction = "seatClicked(" + seatID + ", "
					+ userID + ", "
					+ layoutID + ", "
					+ partyID + ", "
					+ "'" + elementID.id + "', ";
				if (userReserved) { // un-reserved
					$(elementID).attr("class", "available seat");
					$(elementID).attr("onclick", onClickFunction + "false)");
				} else { // reserved
					$(elementID).attr("class", "userReserved seat");
					$(elementID).attr("onclick", onClickFunction + "true)");
				}
			} else {
				alert("Someone reserved that seat before you!");
			}
		});
}

/*
// User selects a merchandise option
function merchCheck(obj, cost) {
    if (obj.checked)
        totalPrice = totalPrice + cost;
    else
        totalPrice = totalPrice - cost;
    updateCart();
}

function updateCart() {
    // Update price total to user
    sb = "Your current total is: $" + totalPrice + "";
    document.getElementById("MainContent_chosenMessage").innerHTML = sb;

    // set purchase button available if total is greater than 0
    if (seats.length > 0)
        document.getElementById("MainContent_confirm").disabled = false;
    else
        document.getElementById("MainContent_confirm").disabled = true;
}

// User wants to save their seats!
function saveSeats() {
    // Ensure that at least one seat has been selected
    if (seats.length == 0) {
        alert("You haven't selected any seats!");
        return false;
    }

    // Build string to pass to C# via hidden field
    // FORMAT: seatid:seatid:REPEAT
    var sb = "";
    for (var s in seats) {
        sb = sb + seats[s].id + ":";
    }

    // Put that string into the hidden field value
    document.getElementById("MainContent_Hidden1").value = sb;

    return true;
} */