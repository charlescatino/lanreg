﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LANREG.Models;

namespace LANREG.Controllers {

	public class HomeController : _BaseControllerClass {

		public ActionResult Index() {
			HomeModel model = new HomeModel();
			model.RightColumn = new HomeRightColumnModel();
			//TODO: add creation dates on orgs and events
			model.RightColumn.organizations = db.organizations.OrderBy(o => o.name).Take(5).ToList();
			model.RightColumn.parties = db.parties.OrderBy(o => o.name).Take(5).ToList();
			ViewBag.Message = "Supporting all of your LAN party registration needs.";

			return View(model);
		}

		public ActionResult About() {
			ViewBag.Message = "Your app description page.";

			return View();
		}

		public ActionResult Contact() {
			ViewBag.Message = "Your contact page.";

			return View();
		}
	}
}
