﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LANREG.Models;

namespace LANREG.Controllers {
    public class SeatingController : Controller {

		private LANRegistrationEntities db = new LANRegistrationEntities();

        // GET: /Event/{party_url}/Seating/

        public ActionResult Index(string party_url) {
			int party_id;
			// TODO: refactor this into global DAL?
			if (!int.TryParse(party_url, out party_id)) {
				party_id = (from prty in db.parties
							where prty.party_url == party_url
							select prty).Single().id;
			}
			
			SeatingSelectionModel model = new SeatingSelectionModel();
			model.tables = (from tbl in db.tables
							join prty in db.parties on tbl.layoutID equals prty.layoutID
							where prty.id == party_id
							select tbl).ToList();
			model.currUser = (from usr in db.users
							  where usr.id == 1
							  select usr).Single();
			// TODO: determine layoutID based on the partyID! (active layout)
			model.layoutID = 1;
			model.partyID = party_id;
            return View(model);
        }

		/* SHOULD THESE JUST BE IN LAYOUT?  SEATING IS DIFF RIGHT?
		// GET: /Seating/Edit/5

		public ActionResult Edit(int layoutID) {

		}

		// GET: /Seating/Create

		public ActionResult Create() {

		} */
    }
}