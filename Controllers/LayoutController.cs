﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using LANREG.Models;

namespace LANREG.Controllers {
    public class LayoutController : _BaseControllerClass {

        //
		// GET: /{org_url}/Event/{party_url}/Seating/

		public ActionResult Details(string org_url, string party_url) {
			int party_id;
			// TODO: refactor this into global DAL?
			if (!int.TryParse(party_url, out party_id)) {
				party_id = (from prty in db.parties
							where prty.party_url == party_url
							select prty).Single().id;
			}

			SelectSeatingModel model = new SelectSeatingModel();
			model.tables = (from tbl in db.tables
							join prty in db.parties on tbl.layoutID equals prty.layout_id
							where prty.id == party_id
							select tbl).ToList();
			model.currUser = (from usr in db.users
							  where usr.id == 1
							  select usr).Single();
			// TODO: determine layoutID based on the partyID! (active layout)
			model.layoutID = 1;
			model.partyID = party_id;
			return View(model);
		}

        //
        // GET: /Layout/Create

        public ActionResult Create()
        {
			//TODO: Make this only currently logged in user's orgs, and set the default
			// selection to the user's a)referring or b)most active org.
			ViewBag.userOrgsOwnedDDL = new SelectList(db.organizations, "id", "name");
			ViewBag.tableLengthDDL = populateNumList(3, 20, 8);
			ViewBag.tableWidthDDL = populateNumList(1, 2, 2);
			ViewBag.tableOrientationDDL = populateTableOrientation();
			ViewBag.tableStepDDL = populateStep(0);
			ViewBag.seatStepDDL = populateStep(1);
			//TODO: load current user's orgs/layouts and allow edit upon selection?
			ViewBag.currentLayouts = db.layouts.Include(l => l.organization).ToList();

            return View();
        }

        //
        // POST: /Layout/Create

        [HttpPost]
        public bool Create(string jsonData) {
			try {
				if (jsonData == null)
					return false;
				SaveLayoutModel slm;
				JavaScriptSerializer jss = new JavaScriptSerializer();
				slm = jss.Deserialize<SaveLayoutModel>(jsonData);

				int i, numSeats = 0;
				string tableStep, seatStep;

				tableStep = setStep(slm.tableStep);
				seatStep = setStep(slm.seatStep);

				// Create a new layout in the db
				layout lo = new layout {
					name = slm.name,
					numSeats = 0,
					org_id = slm.orgID,
					shared = false
				};
				db.layouts.Add(lo);
				db.SaveChanges();

				// Create all tables and put them in the db
				foreach (table tbl in slm.tables) {
					tbl.layoutID = lo.id;
					tbl.name = tableStep;
					db.tables.Add(tbl);
					tableStep = incrementStep(tableStep);
				}
				db.SaveChanges();

				// Get all tables back out
				var query = (from tbl in db.tables
							 where tbl.layoutID == lo.id
							 select tbl).ToList();

				// Create all of the seats for each table
				foreach (table tbl in query) {
					for (i = 1; i <= tbl.length * tbl.width; i++) {
						seat st = new seat {
							name = seatStep,
							tableID = tbl.id,
							cost = Convert.ToInt32(slm.seatCost)
						};
						db.seats.Add(st);
						seatStep = incrementStep(seatStep);
						numSeats++;
					}
					seatStep = setStep(slm.seatStep);
				}

				lo.numSeats = numSeats;
				db.SaveChanges();
			}
			catch (Exception e) {
				return false;
			}

            return true;
        }

        //
        // GET: /Layout/Edit/5

        public ActionResult Edit(int id = 0)
        {
            layout layout = db.layouts.Find(id);
            if (layout == null)
            {
                return HttpNotFound();
            }
            ViewBag.org_id = new SelectList(db.organizations, "id", "name", layout.org_id);
            return View(layout);
        }

        //
        // POST: /Layout/Edit/5

        [HttpPost]
        public ActionResult Edit(layout layout)
        {
            if (ModelState.IsValid)
            {
                db.Entry(layout).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.org_id = new SelectList(db.organizations, "id", "name", layout.org_id);
            return View(layout);
        }

        //
        // GET: /Layout/Delete/5

        public ActionResult Delete(int id = 0)
        {
            layout layout = db.layouts.Find(id);
            if (layout == null)
            {
                return HttpNotFound();
            }
            return View(layout);
        }

        //
        // POST: /Layout/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            layout layout = db.layouts.Find(id);
            db.layouts.Remove(layout);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
		
		/* HELPER FUNCTIONS */

		private List<SelectListItem> populateNumList(int first, int last, int selectedValue) {
			List<SelectListItem> newList = new List<SelectListItem>();
			int zeroDiff = first;
			for (int i = first; i < (last + 1); i++) {
				SelectListItem sli = new SelectListItem {
						Text = Convert.ToString(i),
						Value = Convert.ToString(i - zeroDiff)
					};
				if (selectedValue == i) {
					sli.Selected = true;
				}
				newList.Add(sli);					
			}
			return newList;
		}

		private List<SelectListItem> populateTableOrientation() {
			List<SelectListItem> newList = new List<SelectListItem>();
			newList.Add(new SelectListItem { Text = "Vertical", Value = "0" });
			newList.Add(new SelectListItem { Text = "Horizontal", Value = "1" });
			return newList;
		}

		private List<SelectListItem> populateStep(int selectedIndex) {
			List<SelectListItem> newList = new List<SelectListItem>();
			newList.Add(new SelectListItem { Text = "Alpha (A-Z)", Value = "0" });
			newList.Add(new SelectListItem { Text = "Numerical (1-#)", Value = "1" });
			newList[selectedIndex].Selected = true;
			return newList;
		}

		private string setStep(int index) {
			// TODO: Error checking for wrong numbers
			switch (index) {
				case 0:
					return "A"; // A -> Z  , TODO: Account for past Z?
				case 1:
					return "1"; // Numerical
			}
			return "";
		}

		protected string incrementStep(string stepValue) {
			// Attempt to convert stepValue to integer format
			int myNum;
			if (int.TryParse(stepValue, out myNum)) {
				myNum++;
				return Convert.ToString(myNum);
			}
			// Attempt to convert to character format
			char myChar;
			if (char.TryParse(stepValue, out myChar)) {
				myChar++;
				return Convert.ToString(myChar);
			}
			return "";
		}
    }
}