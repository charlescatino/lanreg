﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LANREG.Models;

namespace LANREG.Controllers {
	public class _BaseControllerClass : Controller {

		public LANRegistrationEntities db = new LANRegistrationEntities();
		public user currUser;

		protected override void Initialize(System.Web.Routing.RequestContext requestContext) {

			base.Initialize(requestContext);

			if (User.Identity.IsAuthenticated) {
				if (LANSession.Current.currUser != null) {
					currUser = LANSession.Current.currUser;
				}
				else {
					string userName = User.Identity.Name;
					currUser = (from usr in db.users
								where usr.username == userName
								select usr).Single();
					LANSession.Current.currUser = currUser;
				}
			}
			else {
				LANSession.Current.currUser = null;
			}
		}

		public organization updateOrg(int org_id) {
			if (LANSession.Current.currOrg != null && org_id == LANSession.Current.currOrg.id) {
				return LANSession.Current.currOrg;
			}
			else {
				try {
					organization newOrg = getOrgByID(org_id);
					LANSession.Current.currOrg = newOrg;
					return newOrg;
				}
				catch {
					// TODO: LOG THAT ORG WASN'T FOUND
					throw new Exception("An organization with that URL was not found");
				}
			}
		}

		public organization updateOrg(string org_url) {
			if (LANSession.Current.currOrg != null && org_url == LANSession.Current.currOrg.org_url) {
					return LANSession.Current.currOrg;
			} else {
				try {
					organization newOrg = getOrgByURL(org_url);
					LANSession.Current.currOrg = newOrg;
					return newOrg;
				}
				catch {
					// TODO: LOG THAT ORG WASN'T FOUND
					throw new Exception("An organization with that URL was not found");
				}
			}
		}

		public party updateParty(int party_id) {
			if (LANSession.Current.currParty != null && party_id == LANSession.Current.currParty.id) {
				return LANSession.Current.currParty;
			}
			else {
				try {
					party newParty = getPartyByID(party_id);
					LANSession.Current.currParty = newParty;
					return newParty;
				}
				catch {
					// TODO: LOG THAT PARTY WASN'T FOUND
					throw new Exception("An event with that URL was not found");
				}
			}
		}

		public party updateParty(string party_url) {
			if (LANSession.Current.currParty != null && party_url == LANSession.Current.currParty.party_url) {
				return LANSession.Current.currParty;
			}
			else {
				try {
					party newParty = getPartyByURL(party_url);
					LANSession.Current.currParty = newParty;
					return newParty;
				}
				catch {
					// TODO: LOG THAT PARTY WASN'T FOUND
					throw new Exception("An event with that URL was not found");
				}
			}
		}

		private organization getOrgByID(int org_id) {
			return (from org in db.organizations
					where org.id == org_id
					select org).Single();
		}

		private organization getOrgByURL(string org_url) {
			return (from org in db.organizations
					where org.org_url == org_url
					select org).Single();
		}

		private party getPartyByID(int party_id) {
			return (from prty in db.parties
					where prty.id == party_id
					select prty).Single();
		}

		private party getPartyByURL(string party_url) {
			return (from prty in db.parties
					where prty.party_url == party_url
					select prty).Single();
		}
	}
}
