﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LANREG.Models;

namespace LANREG.Controllers {
    public class PartyController : _BaseControllerClass {

        //
		// GET: /Events

        public ActionResult Index() {
			return View(db.parties.ToList());
        }

        //
        // GET: /{org_url}/Event/{party_url}

        public ActionResult Details(string org_url, string party_url) {
			int party_id;
			// TODO: refactor this into global DAL?
			if (!int.TryParse(party_url, out party_id)) {
				party_id = (from prty in db.parties
							where prty.party_url == party_url
							select prty).Single().id;
			}
            party party = db.parties.Find(party_id);
            if (party == null) {
                return HttpNotFound();
            }
            return View(party);
        }

        //
        // GET: /Create/Party

        public ActionResult Create() {
            ViewBag.layoutID = new SelectList(db.layouts, "id", "name");
            ViewBag.org_id = new SelectList(db.organizations, "id", "name");
            return View();
        }

        //
        // POST: /Create/Party

        [HttpPost]
        public ActionResult Create(party party) {
            if (ModelState.IsValid) {
                db.parties.Add(party);
                db.SaveChanges();
				return RedirectToAction("Details", new { party_url = party.party_url });
            }

            ViewBag.layoutID = new SelectList(db.layouts, "id", "name", party.layout_id);
            ViewBag.org_id = new SelectList(db.organizations, "id", "name", party.org_id);
            return View(party);
        }

        //
        // GET: /Party/Edit/5

        public ActionResult Edit(int id = 0)
        {
            party party = db.parties.Find(id);
            if (party == null)
            {
                return HttpNotFound();
            }
            ViewBag.layoutID = new SelectList(db.layouts, "id", "name", party.layout_id);
            ViewBag.org_id = new SelectList(db.organizations, "id", "name", party.org_id);
            return View(party);
        }

        //
        // POST: /Party/Edit/5

        [HttpPost]
        public ActionResult Edit(party party)
        {
            if (ModelState.IsValid)
            {
                db.Entry(party).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.layoutID = new SelectList(db.layouts, "id", "name", party.layout_id);
            ViewBag.org_id = new SelectList(db.organizations, "id", "name", party.org_id);
            return View(party);
        }

        //
        // GET: /Party/Delete/5

        public ActionResult Delete(int id = 0)
        {
            party party = db.parties.Find(id);
            if (party == null)
            {
                return HttpNotFound();
            }
            return View(party);
        }

        //
        // POST: /Party/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            party party = db.parties.Find(id);
            db.parties.Remove(party);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}

/* TODO: REMOVE
 * OLD CODE:

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LANREG.Models;

namespace LANREG.Controllers
{
    public class EventController : Controller
    {
		LANRegistrationEntities lanDB = new LANRegistrationEntities();

		//GET /Event/?partyid
        public ActionResult Index(int partyID)
        {
			return View(getPartyFromID(partyID));
        }
		
		//POST: /Event/Create
		[HttpPost]
		public ActionResult Create(EventModel em, int partyid) {
			if(Model.IsValid)
				return Content("Hello");
			else
				return Content("Bad Data");
		}

		public ActionResult Seating(int partyID)
		{
			ViewBag.User = getSessionUser();
			return View(getPartyFromID(partyID));
		}

		public ActionResult Pay(int partyID)
		{
			return View();
		}

		private party getPartyFromID(int partyID)
		{
			return (from prty in lanDB.parties
					where prty.id == partyID
					select prty).Single();
		}

		private user getSessionUser()
		{
			if (Session["user"] != null) {
				return (user)Session["user"];
			} else {
				return new user();
			}
		}
    }
}
 */