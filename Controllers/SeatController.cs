﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LANREG.Models;

namespace LANREG.Controllers {
    public class SeatController : _BaseControllerClass {

		//
		// GET: /Seat/Reserve
		
		[HttpPost]
		public bool Reserve(int seatID, int userID, int layoutID, int partyID) {
			var result = (from st_usr in db.seat_user
						   where st_usr.seat_id == seatID
						   && st_usr.active
						   select st_usr);
			if (result.Any()) {
				if (result.Single().user_id == userID) { // un-reserve the seat
					result.Single().active = false;
					db.SaveChanges();
					return true;
				}
				else { // someone else has it
					return false;
				}
			}
			// Reserve that sucker
			seat_user newRes = new seat_user {
				seat_id = seatID,
				user_id = userID,
				layout_id = layoutID,
				party_id = partyID,
				paid = false,
				date_reserved = DateTime.Now,
				active = true
			};
			db.seat_user.Add(newRes);
			db.SaveChanges();
			return true;
		}

        //
        // GET: /Seat/Edit/5

        public ActionResult Edit(int id = 0)
        {
            seat seat = db.seats.Find(id);
            if (seat == null)
            {
                return HttpNotFound();
            }
            ViewBag.tableID = new SelectList(db.tables, "id", "name", seat.tableID);
            return View(seat);
        }

        //
        // POST: /Seat/Edit/5

        [HttpPost]
        public ActionResult Edit(seat seat)
        {
            if (ModelState.IsValid)
            {
                db.Entry(seat).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.tableID = new SelectList(db.tables, "id", "name", seat.tableID);
            return View(seat);
        }
    }
}