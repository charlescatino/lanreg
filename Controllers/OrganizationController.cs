﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LANREG.Models;

namespace LANREG.Controllers {

	[Authorize]
    public class OrganizationController : _BaseControllerClass {

        //
        // GET: /Groups

		[AllowAnonymous]
        public ActionResult Index() {
            return View(db.organizations.ToList());
        }

        //
        // GET: /{org_url}

		[AllowAnonymous]
		public ActionResult Details(string org_url) {
			ViewOrganizationModel model = new ViewOrganizationModel();
			model.org = updateOrg(org_url);
			model.nextEvents = getUpcomingEvents(model.org.id);
			model.recentEvents = getRecentEvents(model.org.id);
            return View(model);
        }

		//
		// GET: /{org_url}/Events

		[AllowAnonymous]
		public ActionResult Events(string org_url) {
			updateOrg(org_url);
			var parties = (from prty in db.parties
						   where prty.org_id == LANSession.Current.currOrg.id
						   select prty).ToList();
			return View(parties);
		}

        //
        // GET: /NewGroup

        public ActionResult Create() {
            return View();
        }

        //
        // POST: /NewGroup

        [HttpPost]
        public ActionResult Create(organization organization) {
            if (ModelState.IsValid) {
				db.organizations.Add(organization);
				db.SaveChanges();

				// Add current user as admin to new group
				org_access admin = new org_access {
					org_id = organization.id,
					level = 1,
					user_id = currUser.id
				};
				db.org_access.Add(admin);
				db.SaveChanges();
				return RedirectToAction("Details", new { org_url = organization.org_url });
            }

            return View(organization);
        }

        //
        // GET: /Organization/Edit/5

        public ActionResult Edit(int id = 0) {
            organization organization = db.organizations.Find(id);
            if (organization == null) {
                return HttpNotFound();
            }
            return View(organization);
        }

        //
        // POST: /Organization/Edit/5

        [HttpPost]
        public ActionResult Edit(organization organization) {
            if (ModelState.IsValid) {
                db.Entry(organization).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(organization);
        }

		//
		// POST: /Organization/UpdateName

		[HttpPost]
		public bool UpdateName() {

			return true;
		}

		/* DATA LAYER ACCESS */

		private List<party> getRecentEvents(int org_id) {
			return (from prty in db.parties
					where prty.org_id == org_id
					&& prty.date_time_end < DateTime.Today
					orderby prty.date_time_start descending
					select prty).Take(5).ToList();
		}

		private List<party> getUpcomingEvents(int org_id) {
			return (from prty in db.parties
					where prty.org_id == org_id
					&& prty.date_time_start > DateTime.Today
					select prty).ToList();
		}
    }
}