﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text;
using System.Net;
using LANREG.Models;
using System.Net.Mail;

namespace LANREG.Controllers
{
	public class LayoutTESTINGController : Controller {
		private LANRegistrationEntities db = new LANRegistrationEntities();

		//
		// GET: /LayoutTESTING/

		public ActionResult Index() {
			var layouts = db.layouts.Include(l => l.organization);
			return View(layouts.ToList());
		}

		//
		// GET: /LayoutTESTING/Details/5

		public ActionResult Details(int id = 0) {
			layout layout = db.layouts.Find(id);
			if (layout == null) {
				return HttpNotFound();
			}
			return View(layout);
		}

		//
		// GET: /LayoutTESTING/Create

		public ActionResult Create() {
			ViewBag.org_id = new SelectList(db.organizations, "id", "name");
			return View();
		}

		//
		// POST: /LayoutTESTING/Create

		[HttpPost]
		public ActionResult Create(layout layout) {
			if (ModelState.IsValid) {
				db.layouts.Add(layout);
				db.SaveChanges();
				return RedirectToAction("Index");
			}

			ViewBag.org_id = new SelectList(db.organizations, "id", "name", layout.org_id);
			return View(layout);
		}

		//
		// GET: /LayoutTESTING/Edit/5

		public ActionResult Edit(int id = 0) {
			layout layout = db.layouts.Find(id);
			if (layout == null) {
				return HttpNotFound();
			}
			ViewBag.org_id = new SelectList(db.organizations, "id", "name", layout.org_id);
			return View(layout);
		}

		//
		// POST: /LayoutTESTING/Edit/5

		[HttpPost]
		public ActionResult Edit(layout layout) {
			if (ModelState.IsValid) {
				db.Entry(layout).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			ViewBag.org_id = new SelectList(db.organizations, "id", "name", layout.org_id);
			return View(layout);
		}

		//
		// GET: /LayoutTESTING/Delete/5

		public ActionResult Delete(int id = 0) {
			layout layout = db.layouts.Find(id);
			if (layout == null) {
				return HttpNotFound();
			}
			return View(layout);
		}

		//
		// POST: /LayoutTESTING/Delete/5

		[HttpPost, ActionName("Delete")]
		public ActionResult DeleteConfirmed(int id) {
			layout layout = db.layouts.Find(id);
			db.layouts.Remove(layout);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing) {
			db.Dispose();
			base.Dispose(disposing);
		}

		// POST: IPN

		public void IPN() {
			// email to this person
			string bossEmail = "catinoc@onid.orst.edu";

			//Post back to either sandbox or live
			string strSandbox = "https://www.sandbox.paypal.com/cgi-bin/webscr";
			string strLive = "https://www.paypal.com/cgi-bin/webscr";
			HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strSandbox);

			//Set values for the request back
			req.Method = "POST";
			req.ContentType = "application/x-www-form-urlencoded";
			byte[] param = Request.BinaryRead(HttpContext.Request.ContentLength);
			string strRequest = Encoding.ASCII.GetString(param);
			strRequest += "&cmd=_notify-validate";
			req.ContentLength = strRequest.Length;

			//for proxy
			//WebProxy proxy = new WebProxy(new Uri("http://url:port#"));
			//req.Proxy = proxy;

			//Send the request to PayPal and get the response
			StreamWriter streamOut = new StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII);
			streamOut.Write(strRequest);
			streamOut.Close();
			StreamReader streamIn = new StreamReader(req.GetResponse().GetResponseStream());
			string strResponse = streamIn.ReadToEnd();
			streamIn.Close();

			if (strResponse == "VERIFIED") {
				SendEmail(bossEmail, "admin@mcnarylan.com", "VERIFIED?", "Someone tried, didn't work");
				//check the payment_status is Completed
				//check that txn_id has not been previously processed
				//check that receiver_email is your Primary PayPal email
				//check that payment_amount/payment_currency are correct
				//process payment
			}
			else if (strResponse == "INVALID") {
				SendEmail(bossEmail, "admin@mcnarylan.com", "Invalid IPN attempt", "Someone tried, didn't work");
				//log for manual investigation
			}
			else {
				SendEmail(bossEmail, "admin@mcnarylan.com", "Paypal IPN didn't respond", "Read the subject");
				//log response/ipn data for manual investigation
			}
		}

		public static void SendEmail(string toEmail, string fromEmail, string subject, string message) {
			SmtpClient mailClient = new SmtpClient("mail.oregonstate.edu", 587);
			mailClient.EnableSsl = true;
			mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
			mailClient.UseDefaultCredentials = false;
			mailClient.Credentials = new NetworkCredential("catinoc@onid.orst.edu", "PENdesk762#");
			mailClient.Send("donotreply@lanreg.azurewebsites.net",
							toEmail,
							subject,
							message);
			/*MailAddress addr = new MailAddress(toEmail);
			MailAddress from = new MailAddress(fromEmail);
			System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
			msg.Sender = from;
			msg.To.Add(addr);
			msg.From = from;
			msg.Subject = subject;
			msg.Body = message;
			msg.IsBodyHtml = true;

			SmtpClient client = new SmtpClient();
			client.Send(msg); */
		}
	}
}