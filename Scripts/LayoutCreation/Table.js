﻿/*
    Table

    Intended to mirror a table database object.
    This will make life easier when pulling the data from the tables.

    @orientation
    @length
    @id
    @xCoord
    @yCoord

*/

function Table(or, len, wid, idTag) {
    this.orientation = or;
    this.length = len;
    this.width = wid;
    this.id = idTag;
    this.leftOffset = 0;
    this.topOffset = 0;

    this.setCoords = function (x, y) {
    	this.leftOffset = x;
    	this.topOffset = y;
    }
}