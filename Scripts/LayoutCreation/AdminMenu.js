﻿var isVisible = false;

function setAdminMenu() {
    $("#AdminMenu_ul").hide();
}

function clickAdminMenu(obj) {
    $("#AdminMenu_ul").slideToggle("slow");

    if (isVisible) {
        isVisible = false;
        $(obj).text("[+] Admin");
    }
    else {
        isVisible = true;
        $(obj).text("[-] Admin");
    }
}