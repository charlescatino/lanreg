﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LANREG.Models;

/// <summary>
/// Summary description for LANSession
/// Stolen from here: http://stackoverflow.com/questions/621549/how-to-access-session-variables-from-any-class-in-asp-net
/// </summary>
public class LANSession {

	public party currParty { get; set; }
	public organization currOrg { get; set; }
	public user currUser { get; set; }

	// private constructor
	public LANSession() {

	}

	// Gets the current session.
	public static LANSession Current {
		get {
			LANSession session = (LANSession)HttpContext.Current.Session["__LANSession__"];
			if (session == null) {
				session = new LANSession();
				HttpContext.Current.Session["__LANSession__"] = session;
			}
			return session;
		}
	}
}